Database = require('better-sqlite3')

class TagDB

  constructor: (dbFile) ->
    @db = new Database(dbFile)
    console.log("Opened Database #{dbFile}")

  tags: ->
    @db.prepare("SELECT id,tag FROM tags").all().map( (row) => row.tag )

  tagsForFile: (fileId) ->
    @db.prepare("SELECT t.tag AS tag FROM filetags ft LEFT JOIN tags t ON ft.tag_id=t.id WHERE file_id=?")
      .all(fileId).map( (row) => row.tag )

  fileForId: (fileId) ->
    @db.prepare("SELECT f.name AS name FROM files f WHERE f.id=?")
      .get(fileId).name

  files: ->
    @db.prepare("SELECT id,name FROM files").all()

  close: ->
    @db.close()

export default TagDB
